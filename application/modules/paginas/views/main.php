<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
</div>
<!-- Content-->
<div class="template-content">

    <!-- Slider -->
    <div class="template-content-section template-padding-reset">

        <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#FAF4E7;padding:0px;margin-top:0px;margin-bottom:0px;">

            <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.1.6">

                <ul>

                    <li data-index="rs-1" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?= base_url() ?>img/_sample/slider/slider_slide1_background-100x50.png" data-rotate="0" data-saveperformance="off" class="fable_slide_1_class" id="fable_slide_1_id" data-title="fable_slide_1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                        <img src="<?= base_url() ?>img/_sample/slider/slider_slide1_background.png" alt="" width="100" height="600" data-bgposition="center top" data-bgfit="contain" data-bgrepeat="repeat-x" class="rev-slidebg" data-no-retina>

                        <!-- Layer nr. 1 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-1"
                             data-x="-50"
                             data-y="73"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:-80;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                             data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                             data-start="300"
                             data-responsive_offset="on"
                             style="z-index:5;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide1_img1.png" alt="" width="580" height="491" data-no-retina>
                        </div>

                        <!-- Layer nr. 2 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-2"
                             data-x="right" data-hoffset="-55"
                             data-y="44"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:80;y:0;z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1200;e:Power3.easeInOut;"
                             data-transform_out="x:50px;opacity:0;s:300;s:300;"
                             data-start="800"
                             data-responsive_offset="on"
                             style="z-index:6;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide1_img2.png" alt="" width="567" height="546" data-no-retina>
                        </div>

                        <!-- Layer nr. 3 -->
                        <div class="tp-caption tp-font-handlee tp-resizeme"
                             id="slide-4-layer-3"
                             data-x="100"
                             data-y="center" data-voffset="-102"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1400;e:Power3.easeInOut;"
                             data-transform_out="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;"
                             data-start="800"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index:7;white-space:nowrap;color:#a22f1d;border-color:#848484;margin-bottom: 50px">Ensenyament de qualitat
                        </div>

                        <!-- Layer nr. 4 -->
                        <div class="tp-caption tp-font-lato-black tp-resizeme"
                             id="slide-4-layer-4"
                             data-x="100"
                             data-y="center" data-voffset="-45"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;"
                             data-start="1600"
                             data-splitin="words"
                             data-splitout="words"
                             data-responsive_offset="on"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             style="z-index:8;white-space:nowrap;border-color:#848484;">Llar d'infants i infantil
                        </div>

                        <!-- Layer nr. 5 -->
                        <div class="tp-caption tp-font-lato-light tp-resizeme"
                             id="slide-4-layer-5"
                             data-x="100"
                             data-y="center" data-voffset="15"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;"
                             data-start="2100"
                             data-splitin="words"
                             data-splitout="words"
                             data-responsive_offset="on"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             style="z-index:9;white-space:nowrap;border-color:#848484;">Escola de confiança
                        </div>

                        <!-- Layer nr. 6 -->
                        <div class="tp-caption tp-slider-button tp-resizeme"
                             id="slide-4-layer-6"
                             data-x="100"
                             data-y="center" data-voffset="105"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:1.2;sY:1.2;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;"
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;"
                             data-start="2600"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index:10;">
                            <a href="http://themeforest.net/user/quanticalabs/portfolio?ref=quanticalabs" target="_top" class="template-component-button template-component-button-style-1">Més informació</a>
                        </div>

                        <!-- Layer nr. 7 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-7"
                             data-x="-95"
                             data-y="266"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="2600"
                             data-responsive_offset="on"
                             style="z-index:11;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon1.png" alt="" width="98" height="100" data-no-retina>
                        </div>

                        <!-- Layer nr. 8 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-8"
                             data-x="-25"
                             data-y="46"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="2800"
                             data-responsive_offset="on"
                             style="z-index:12;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon2.png" alt="" width="88" height="87" data-no-retina>
                        </div>

                        <!-- Layer nr. 9 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-9"
                             data-x="310"
                             data-y="70"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="3000"
                             data-responsive_offset="on"
                             style="z-index:13;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon3.png" alt="" width="92" height="91" data-no-retina>
                        </div>

                        <!-- Layer nr. 10 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-10"
                             data-x="292"
                             data-y="265"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="3200"
                             data-responsive_offset="on"
                             style="z-index:14;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon4.png" alt="" width="74" height="85" data-no-retina>
                        </div>

                        <!-- Layer nr. 11 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-11"
                             data-x="865"
                             data-y="270"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="2600"
                             data-responsive_offset="on"
                             style="z-index:15;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon7.png" alt="" width="73" height="81" data-no-retina>
                        </div>

                        <!-- Layer nr. 12 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-12"
                             data-x="840"
                             data-y="120"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="2800"
                             data-responsive_offset="on"
                             style="z-index:16;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon6.png" alt="" width="86" height="77" data-no-retina>
                        </div>

                        <!-- Layer nr. 13 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-13"
                             data-x="700"
                             data-y="40"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="3000"
                             data-responsive_offset="on"
                             style="z-index:17;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon5.png" alt="" width="99" height="98" data-no-retina>
                        </div>

                        <!-- Layer nr. 14 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-14"
                             data-x="1170"
                             data-y="35"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="3200"
                             data-responsive_offset="on"
                             style="z-index:18;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon8.png" alt="" width="86" height="85" data-no-retina>
                        </div>

                        <!-- Layer nr. 15 -->
                        <div class="tp-caption tp-resizeme"
                             id="slide-1-layer-15"
                             data-x="1230"
                             data-y="200"
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;"
                             data-transform_out="auto:auto;s:300;"
                             data-start="3400"
                             data-responsive_offset="on"
                             style="z-index:19;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon9.png" alt="" width="74" height="91" data-no-retina>
                        </div>

                    </li>

                    <!-- SLIDE -->
                    <li data-index="rs-4" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?= base_url() ?>img/_sample/slider/slider_slide2_background-100x50.png" data-rotate="0" data-saveperformance="off" class="fable_slide_2_class" id="fable_slide_2_id" data-title="fable_slide_2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                        <img src="<?= base_url() ?>img/_sample/slider/slider_slide2_background.png" alt="" width="100" height="600" data-bgposition="center top" data-bgfit="contain" data-bgrepeat="repeat-x" class="rev-slidebg" data-no-retina>

                        <!-- Layer nr. 1 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-1" 
                             data-x="850" 
                             data-y="57" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:50px;opacity:0;s:1800;e:Power3.easeInOut;" 
                             data-transform_out="x:50px;opacity:0;s:300;s:300;" 
                             data-start="900" 
                             data-responsive_offset="on" 
                             style="z-index:5;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide2_img2.png" alt="" width="453" height="458" data-no-retina> 
                        </div>

                        <!-- Layer nr. 2 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-2" 
                             data-x="540" 
                             data-y="34" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:200;y:0;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1800;e:easeInOutExpo;" 
                             data-transform_out="x:-50px;opacity:0;s:300;s:300;" 
                             data-start="300" 
                             data-responsive_offset="on" 
                             style="z-index:6;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide2_img1.png" alt="" width="465" height="537" data-no-retina> 
                        </div>

                        <!-- Layer nr. 3 -->
                        <div class="tp-caption tp-font-handlee tp-resizeme" 
                             id="slide-4-layer-3" 
                             data-x="100" 
                             data-y="center" data-voffset="-102" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1400;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="800" 
                             data-splitin="none" 
                             data-splitout="none" 
                             data-responsive_offset="on" 
                             style="z-index:7;white-space:nowrap;color:#a22f1d;border-color:#848484; margin-bottom: 50px">Ensenyament de qualitat
                        </div>

                        <!-- Layer nr. 4 -->
                        <div class="tp-caption tp-font-lato-black tp-resizeme" 
                             id="slide-4-layer-4" 
                             data-x="100" 
                             data-y="center" data-voffset="-45" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="1600" 
                             data-splitin="words" 
                             data-splitout="words" 
                             data-responsive_offset="on" 
                             data-elementdelay="0.1" 
                             data-endelementdelay="0.1" 
                             style="z-index:8;white-space:nowrap;border-color:#848484;">Primària
                        </div>

                        <!-- Layer nr. 5 -->
                        <div class="tp-caption tp-font-lato-light tp-resizeme" 
                             id="slide-4-layer-5" 
                             data-x="100" 
                             data-y="center" data-voffset="15" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="2100" 
                             data-splitin="words" 
                             data-splitout="words" 
                             data-responsive_offset="on" 
                             data-elementdelay="0.1" 
                             data-endelementdelay="0.1" 
                             style="z-index:9;white-space:nowrap;border-color:#848484;">Escola de confiança
                        </div>

                        <!-- Layer nr. 6 -->
                        <div class="tp-caption tp-slider-button tp-resizeme" 
                             id="slide-4-layer-6" 
                             data-x="100" 
                             data-y="center" data-voffset="105" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:1.2;sY:1.2;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="2600" 
                             data-splitin="none" 
                             data-splitout="none" 
                             data-responsive_offset="on" 
                             style="z-index:10;">
                            <a href="http://themeforest.net/user/quanticalabs/portfolio?ref=quanticalabs" target="_top" class="template-component-button template-component-button-style-1">Més informació</a>
                        </div>

                        <!-- Layer nr. 7 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-7" 
                             data-x="660" 
                             data-y="50" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="2600" 
                             data-responsive_offset="on" 
                             style="z-index:11;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon13.png" alt="" width="83" height="93" data-no-retina> 
                        </div>

                        <!-- Layer nr. 8 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-8" 
                             data-x="550" 
                             data-y="120" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="2800" 
                             data-responsive_offset="on" 
                             style="z-index:12;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon12.png" alt="" width="89" height="78" data-no-retina> 
                        </div>

                        <!-- Layer nr. 9 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-9" 
                             data-x="470" 
                             data-y="40" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3000" 
                             data-responsive_offset="on" 
                             style="z-index:13;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon11.png" alt="" width="81" height="82" data-no-retina> 
                        </div>

                        <!-- Layer nr. 10 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-10" 
                             data-x="425" 
                             data-y="130" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3200" 
                             data-responsive_offset="on" 
                             style="z-index:14;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon14.png" alt="" width="61" height="43" data-no-retina> 
                        </div>

                        <!-- Layer nr. 11 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-4-layer-11" 
                             data-x="320" 
                             data-y="35" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3400" 
                             data-responsive_offset="on" 
                             style="z-index:15;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon10.png" alt="" width="79" height="84" data-no-retina> 
                        </div>

                    </li>

                    <li data-index="rs-5" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="<?= base_url() ?>img/_sample/slider/slider_slide3_background-100x50.png" data-rotate="0" data-saveperformance="off" class="fable_slide_3_class" id="fable_slide_3_id" data-title="fable_slide_3" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                        <img src="<?= base_url() ?>img/_sample/slider/slider_slide3_background.png" alt="" width="100" height="600" data-bgposition="center top" data-bgfit="contain" data-bgrepeat="repeat-x" class="rev-slidebg" data-no-retina>

                        <!-- Layer nr. 1 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-1" 
                             data-x="610" 
                             data-y="70" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:-30;y:0;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1800;e:Power3.easeInOut;" 
                             data-transform_out="x:50px;opacity:0;s:300;s:300;" 
                             data-start="900" 
                             data-responsive_offset="on" 
                             style="z-index:5;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide3_img1.png" alt="" width="607" height="442" data-no-retina> 
                        </div>

                        <!-- Layer nr. 2 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-2" 
                             data-x="425" 
                             data-y="375" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:-50px;opacity:0;s:1400;e:Power3.easeInOut;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="300" 
                             data-responsive_offset="on" 
                             style="z-index:6;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide3_img2.png" alt="" width="254" height="157" data-no-retina> 
                        </div>

                        <!-- Layer nr. 3 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-3" 
                             data-x="1080" 
                             data-y="435" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:50px;opacity:0;s:1600;e:Power3.easeInOut;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="300" 
                             data-responsive_offset="on" 
                             style="z-index:7;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_slide3_img3.png" alt="" width="172" height="97" data-no-retina> 
                        </div>

                        <!-- Layer nr. 4 -->
                        <div class="tp-caption tp-font-handlee tp-resizeme" 
                             id="slide-5-layer-4" 
                             data-x="100" 
                             data-y="center" data-voffset="-102" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1400;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:-20;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="800" 
                             data-splitin="none" 
                             data-splitout="none" 
                             data-responsive_offset="on" 
                             style="z-index:8;white-space:nowrap;color:#a22f1d;border-color:#848484; margin-bottom: 50px">Ensenyament de qualitat
                        </div>

                        <!-- Layer nr. 5 -->
                        <div class="tp-caption tp-font-lato-black tp-resizeme" 
                             id="slide-5-layer-5" 
                             data-x="100" 
                             data-y="center" data-voffset="-45" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="1600" 
                             data-splitin="words" 
                             data-splitout="words" 
                             data-responsive_offset="on" 
                             data-elementdelay="0.1" 
                             data-endelementdelay="0.1" 
                             style="z-index:9;white-space:nowrap;border-color:#848484;">Secundària
                        </div>

                        <!-- Layer nr. 6 -->
                        <div class="tp-caption tp-font-lato-light tp-resizeme" 
                             id="slide-5-layer-6" 
                             data-x="100" 
                             data-y="center" data-voffset="15" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="2100" 
                             data-splitin="words" 
                             data-splitout="words" 
                             data-responsive_offset="on" 
                             data-elementdelay="0.1" 
                             data-endelementdelay="0.1" 
                             style="z-index:10;white-space:nowrap;border-color:#848484;">Escola de confiança
                        </div>

                        <!-- Layer nr. 7 -->
                        <div class="tp-caption tp-slider-button tp-resizeme" 
                             id="slide-5-layer-7" 
                             data-x="100" 
                             data-y="center" data-voffset="105" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:0;z:0;rX:0;rY:0;rZ:0;sX:1.2;sY:1.2;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" 
                             data-transform_out="x:0;y:50;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:300;s:300;" 
                             data-start="2600" 
                             data-splitin="none" 
                             data-splitout="none" 
                             data-responsive_offset="on" 
                             style="z-index:11;">
                            <a href="http://themeforest.net/user/quanticalabs/portfolio?ref=quanticalabs" target="_top" class="template-component-button template-component-button-style-1">Més informació</a>
                        </div>

                        <!-- Layer nr. 8 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-8" 
                             data-x="526" 
                             data-y="126" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="2600" 
                             data-responsive_offset="on" 
                             style="z-index:12;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon15.png" alt="" width="87" height="84" data-no-retina> 
                        </div>

                        <!-- Layer nr. 9 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-9" 
                             data-x="430" 
                             data-y="40" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="2800" 
                             data-responsive_offset="on" 
                             style="z-index:13;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon16.png" alt="" width="79" height="84" data-no-retina> 
                        </div>

                        <!-- Layer nr. 10 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-10" 
                             data-x="450" 
                             data-y="150" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3000" 
                             data-responsive_offset="on" 
                             style="z-index:14;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon14.png" alt="" width="61" height="43" data-no-retina> 
                        </div>

                        <!-- Layer nr. 11 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-11" 
                             data-x="625" 
                             data-y="50" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3200" 
                             data-responsive_offset="on" 
                             style="z-index:15;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon17.png" alt="" width="65" height="73" data-no-retina> 
                        </div>

                        <!-- Layer nr. 12 -->
                        <div class="tp-caption tp-resizeme" 
                             id="slide-5-layer-12" 
                             data-x="1020" 
                             data-y="30" 
                             data-width="auto"
                             data-height="auto"
                             data-transform_idle=""
                             data-transform_in="x:0;y:100;z:0;rX:0;rY:0;rZ:0;sX:0.5;sY:0.5;skX:0;skY:0;opacity:0;s:1500;e:easeInOutExpo;" 
                             data-transform_out="auto:auto;s:300;" 
                             data-start="3400" 
                             data-responsive_offset="on" 
                             style="z-index:16;">
                            <img src="<?= base_url() ?>img/_sample/slider/slider_icon18.png" alt="" width="78" height="76" data-no-retina> 
                        </div>

                    </li>

                </ul>

                <div class="tp-bannertimer tp-bottom" style="visibility:hidden !important;"></div>	

            </div>

            <script type="text/javascript">

                var setREVStartSize = function ()
                {
                    try
                    {
                        var e = new Object, i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                        e.c = jQuery('#rev_slider_1_1');
                        e.gridwidth = [1250];
                        e.gridheight = [600];

                        e.sliderLayout = "fullwidth";
                        if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                            f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                            var u = (e.c.width(), jQuery(window).height());
                            if (void 0 != e.fullScreenOffsetContainer) {
                                var c = e.fullScreenOffsetContainer.split(",");
                                if (c)
                                    jQuery.each(c, function (e, i) {
                                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                            }
                            f = u
                        } else
                            void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                        e.c.closest(".rev_slider_wrapper").css({height: f})
                    }
                    catch (d)
                    {
                        console.log("Failure at Presize of Slider:" + d)
                    }
                };

                setREVStartSize();
                function revslider_showDoubleJqueryError(sliderID)
                {
                    var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                    errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                    errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                    errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                    errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                    jQuery(sliderID).show().html(errorMessage);
                }

                var tpj = jQuery;

                tpj.noConflict();

                var revapi1;

                tpj(document).ready(function ()
                {
                    if (tpj("#rev_slider_1_1").revolution == undefined)
                    {
                        revslider_showDoubleJqueryError("#rev_slider_1_1");
                    }
                    else
                    {
                        revapi1 = tpj("#rev_slider_1_1").show().revolution(
                                {
                                    sliderType: "standard",
                                    sliderLayout: "fullwidth",
                                    dottedOverlay: "none",
                                    delay: 6000,
                                    navigation:
                                            {
                                                keyboardNavigation: "off",
                                                keyboard_direction: "horizontal",
                                                mouseScrollNavigation: "off",
                                                onHoverStop: "off",
                                                touch:
                                                        {
                                                            touchenabled: "on",
                                                            swipe_threshold: 75,
                                                            swipe_min_touches: 1,
                                                            swipe_direction: "horizontal",
                                                            drag_block_vertical: false
                                                        },
                                                arrows:
                                                        {
                                                            style: "hesperiden",
                                                            enable: true,
                                                            hide_onmobile: false,
                                                            hide_onleave: false,
                                                            tmp: '',
                                                            left:
                                                                    {
                                                                        h_align: "left",
                                                                        v_align: "center",
                                                                        h_offset: 20,
                                                                        v_offset: 0
                                                                    },
                                                            right:
                                                                    {
                                                                        h_align: "right",
                                                                        v_align: "center",
                                                                        h_offset: 20,
                                                                        v_offset: 0
                                                                    }
                                                        },
                                                bullets:
                                                        {
                                                            enable: true,
                                                            hide_onmobile: false,
                                                            style: "hesperiden",
                                                            hide_onleave: false,
                                                            direction: "horizontal",
                                                            h_align: "center",
                                                            v_align: "bottom",
                                                            h_offset: 0,
                                                            v_offset: 30,
                                                            space: 10,
                                                            tmp: ''
                                                        }
                                            },
                                    visibilityLevels: [1240, 1024, 778, 480],
                                    gridwidth: 1250,
                                    gridheight: 600,
                                    lazyType: "none",
                                    shadow: 0,
                                    spinner: "spinner2",
                                    stopLoop: "off",
                                    stopAfterLoops: -1,
                                    stopAtSlide: -1,
                                    shuffle: "off",
                                    autoHeight: "off",
                                    disableProgressBar: "on",
                                    hideThumbsOnMobile: "off",
                                    hideSliderAtLimit: 0,
                                    hideCaptionAtLimit: 0,
                                    hideAllCaptionAtLilmit: 0,
                                    debugMode: false,
                                    fallbacks:
                                            {
                                                simplifyAll: "off",
                                                nextSlideOnWindowFocus: "off",
                                                disableFocusListener: false
                                            }
                                });
                    }
                });

            </script>

        </div>						

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-reset template-background-image">

        <!-- Main -->
        <div class="template-main">



            <!-- Call to action -->

            <div data-id="section-2">
                <!-- Twitter user timeline -->
                <div class="template-section-white">
                    <div class="template-component-twitter-user-timeline template-component-twitter-user-timeline-style-2" style="overflow: hidden; width: 1050px; opacity: 1;">

                        <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 1050px; height: 70px; margin: 0px; overflow: hidden; cursor: move;">
                            <ul class="template-layout-100" style="width: 11550px; height: 325px; text-align: left; float: none; position: absolute; top: 16px; right: auto; bottom: auto; left: 0px; margin: 0px; z-index: auto;">

                                <li class="template-layout-column-left" style="float: left; clear: none; margin: 0px; width: 1050px; visibility: visible;">
                                    <i></i>
                                    <p>New version (1.6) of our #Car #Wash #Booking System For #WordPress is available for download: https://t.co/0GhcDc3ObZ</p>
                                    <div></div>

                                </li><li class="template-layout-column-left" style="float: left; clear: none; margin: 0px; width: 1050px; visibility: visible;">
                                    <i></i>
                                    <p>New version (v1.6) of #Auto #Spa - #Car #Wash #Auto #Detail #WordPress #Theme is available: https://t.co/fM9UeUP4ne</p>
                                    <div></div>

                                </li><li class="template-layout-column-left" style="float: left; clear: none; margin: 0px; width: 1050px; visibility: visible;">
                                    <i></i>
                                    <p>New version (1.5) of our #Car #Wash #Booking System For #WordPress is available for download: https://t.co/0GhcDc3ObZ</p>
                                    <div></div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Benvinguts a Monalco</h2>
                <h6>Més de 80 anys aprenent junts</h6>
                <div></div>
            </div>	

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-4 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt"></div>
                        <h5>Llar d'infants</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-center-left">
                        <div class="template-icon-feature template-icon-feature-name-blocks-alt"></div>
                        <h5>Infantil</h5>
                        <p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>		
                    </li>	
                    <li class="template-layout-column-center-right">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt"></div>
                        <h5>Primària</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-keyboard-alt"></div>
                        <h5>Secundària</h5>
                        <p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>			
                    </li>	
                </ul>
            </div>							

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <!-- Tab -->
                    <div class="template-component-tab">
                        <ul class="template-align-left">
                            <li>
                                <a href="#template-tab-1">Filosofia</a>
                                <span></span>
                            </li>
                            <li>
                                <a href="#template-tab-2">Missió</a>
                                <span></span>
                            </li>
                        </ul>
                        <div id="template-tab-1">

                            <h4>Our philosophy is learning through play as we offer a stimulating environment for children.</h4>

                            <!-- Feature -->
                            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                                <ul class="template-layout-100 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                                        <h5>Full Day Sessions</h5>
                                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum. Maecenas node estibulum.</p>
                                    </li>
                                    <li class="template-layout-column-left">
                                        <div class="template-icon-feature template-icon-feature-name-diagram-alt"></div>
                                        <h5>Varied Classes</h5>
                                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum. Maecenas node estibulum.</p>		
                                    </li>	
                                </ul>
                            </div>	

                        </div>
                        <div id="template-tab-2">

                            <!-- Feature -->
                            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
                                <ul class="template-layout-100 template-clear-fix">
                                    <li class="template-layout-column-left">
                                        <div class="template-icon-feature template-icon-feature-name-screen"></div>
                                        <h5>Online Access</h5>
                                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum. Maecenas node estibulum.</p>
                                    </li>
                                    <li class="template-layout-column-left">
                                        <div class="template-icon-feature template-icon-feature-name-heart"></div>
                                        <h5>Maecenas Node</h5>
                                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum. Maecenas node estibulum.</p>		
                                    </li>
                                    <li class="template-layout-column-left">
                                        <div class="template-icon-feature template-icon-feature-name-lab"></div>
                                        <h5>Praesent Morbi</h5>
                                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum. Maecenas node estibulum.</p>		
                                    </li>	
                                </ul>
                            </div>			

                        </div>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <!-- Nivo slider -->
                    <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                        <div>
                            <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/10.jpg" alt=""/>
                            <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/5.jpg" alt=""/>
                            <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/11.jpg" alt=""/>
                        </div>
                    </div>						

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-1">

        <!-- Main -->
        <div class="template-main">

            <div class="template-section-white">
                <div class="template-component-counter-box template-state-carousel-enable">
                    <ul class="template-layout-25x25x25x25 template-clear-fix">
                        <li class="template-layout-column-left">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-value">507</span>
                            </span>
                            <h5>Alumnes</h5>
                            <p>Pulvinar forte maestro node terminal est elipsis prism.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                        <li class="template-layout-column-center-left">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-value">235</span>
                            </span>
                            <h5>Professors</h5>
                            <p>Elipsis morbi nulla a metro interdum vitae elite.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                        <li class="template-layout-column-center-right">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-value">100</span>
                                <span class="template-component-counter-box-counter-character">%</span>
                            </span>
                            <h5>Projectes</h5>
                            <p>Elementum pulvinar detos diaspis movum blandit.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                        <li class="template-layout-column-right">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-character"></span>
                                <span class="template-component-counter-box-counter-value">1050</span>
                            </span>
                            <h5>Hores de classes</h5>
                            <p>Pulvinar forte maestro node terminal est elipsis prism.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                        <li class="template-layout-column-left">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-character"></span>
                                <span class="template-component-counter-box-counter-value">15</span>
                            </span>
                            <h5>Morning Session</h5>
                            <p>Elipsis morbi nulla a metro interdum vitae elite.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                        <li class="template-layout-column-center-left">
                            <span class="template-component-counter-box-counter">
                                <span class="template-component-counter-box-counter-character"></span>
                                <span class="template-component-counter-box-counter-value">25</span>
                            </span>
                            <h5>Full Daycare</h5>
                            <p>Elementum pulvinar detos diaspis movum blandit.</p>
                            <span class="template-component-counter-box-timeline template-state-hidden">
                                <span></span>
                            </span>
                        </li>
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>				
        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Projectes de l'escola</h2>
                <h6>Our preschool program has four dedicated classes</h6>
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">
                    <h4>We provided four classes with nine to twenty children each aged twelve month to five years of age.</h4>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <p>Praesent arcu gravida vehicula est node maecenas loareet morbi a dosis luctus. Urna eget lacinia eleifend praesent luctus a arcu quis facilisis venenatis. Aenean interdum, nibh vitae sodales, magna ante feugiat elit maecenas.</p>
                </div>

            </div>

            <!-- Class -->
            <div class="template-component-class template-clear-fix">

                <!-- Layout 50x50 -->
                <ul class="template-layout-50x50">

                    <!-- Left column -->
                    <li class="template-layout-column-left">

                        <!-- Layout flex 50x50 -->
                        <div class="template-layout-flex-50x50">

                            <!-- Left column -->
                            <div>

                                <!-- Header -->
                                <h5>Escola verda</h5>

                                <!-- Subheader -->
                                <span>Ercu gravida vehicula</span>

                                <!-- Layout flex 50x50 -->
                                <div class="template-layout-flex-50x50">
                                    <div>
                                        <span>12-24</span>
                                        <span>Plà lector</span>
                                    </div>
                                    <div>
                                        <span>9</span>
                                        <span>Class size</span>
                                    </div>

                                </div>
                            </div>

                            <!-- Right column -->
                            <div class="template-component-class-background-1">

                                <!-- Button -->

                                <a href="#" class="template-component-button template-component-button-style-1">llegir més<i></i></a>

                            </div>

                        </div>

                    </li>

                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-layout-flex-50x50">
                            <div>
                                <h5>Pla Lector</h5>
                                <span>Ercu gravida vehicula</span>
                                <div class="template-layout-flex-50x50">
                                    <div>
                                        <span>2-3</span>
                                        <span>Years olds</span>
                                    </div>
                                    <div>
                                        <span>12</span>
                                        <span>Class size</span>
                                    </div>
                                </div>
                            </div>
                            <div class="template-component-class-background-2">
                                <a href="#" class="template-component-button template-component-button-style-1">llegir més<i></i></a>
                            </div>
                        </div>
                    </li>

                    <!-- Left column -->
                    <li class="template-layout-column-left">
                        <div class="template-layout-flex-50x50">
                            <div>
                                <h5>Multilingüe</h5>
                                <span>Ercu gravida vehicula</span>
                                <div class="template-layout-flex-50x50">
                                    <div>
                                        <span>3-4</span>
                                        <span>Years olds</span>
                                    </div>
                                    <div>
                                        <span>15</span>
                                        <span>Class size</span>
                                    </div>
                                </div>
                            </div>
                            <div class="template-component-class-background-3">
                                <a href="#" class="template-component-button template-component-button-style-1">llegir més<i></i></a>
                            </div>
                        </div>
                    </li>



                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-layout-flex-50x50">
                            <div>
                                <h5>APS</h5>
                                <span>Ercu gravida vehicula</span>
                                <div class="template-layout-flex-50x50">
                                    <div>
                                        <span>2-3</span>
                                        <span>Years olds</span>
                                    </div>
                                    <div>
                                        <span>12</span>
                                        <span>Class size</span>
                                    </div>
                                </div>
                            </div>
                            <div class="template-component-class-background-4">
                                <a href="#" class="template-component-button template-component-button-style-1">llegir més<i></i></a>
                            </div>
                        </div>
                    </li>



                </ul>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>We have been educating children for over fifteen years. Our goal is to create a place that engages each child.</h4>

                    <p>Praesent arcu gravida vehicula est node maecenas loareet morbi a dosis luctus. Urna eget lacinia eleifend praesent luctus a arcu quis facilisis venenatis aenean interdum.</p>

                    <!-- List -->
                    <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                        <ul>
                            <li>Comprehensive reporting on individual achievement</li>
                            <li>Educational field trips and school presentations</li>
                            <li>Individual attention in a small-class setting</li>
                            <li>Learning program with after-school care</li>
                            <li>Opportunities to carry out scientific investigations</li>
                            <li>Positive learning environment for your child</li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <!-- Feature -->
                    <div class="template-component-feature template-component-feature-style-6 template-component-feature-position-top template-component-feature-size-medium">
                        <ul class="template-layout-50x50 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-icon-feature template-icon-feature-name-toy"></div>
                                <h5>Learning &amp; Fun</h5>
                                <p>Praesent modea est gravida node vehicula luctus.</p>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-icon-feature template-icon-feature-name-meal"></div>
                                <h5>Healthy Meals</h5>
                                <p>Terminal interdum a eleifend maecenas est morbi.</p>		
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <div class="template-icon-feature template-icon-feature-name-school"></div>
                                <h5>Friendly Place</h5>
                                <p>Terminal interdum a eleifend maecenas est morbi.</p>		
                            </li>	
                            <li class="template-layout-column-right template-margin-bottom-reset">
                                <div class="template-icon-feature template-icon-feature-name-shield"></div>
                                <h5>Children Safety</h5>
                                <p>Praesent modea est gravida node vehicula luctus.</p>		
                            </li>
                        </ul>
                    </div>									

                </div>

            </div>

        </div>

    </div>

    <div class="template-content-section template-padding-reset">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Darreres fotos</h2>
                <h6>Amb educació i experiència en atenció a la primera infància</h6>
            </div>

        </div>

        <!-- Gallery -->
        <div class="template-component-gallery">

            <!-- Layout 25x25x25x25 -->
            <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix">

                <!-- Left column -->
                <li class="template-layout-column-left" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/5.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Center left column -->
                <li class="template-layout-column-center-left" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/6.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/6.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Center right column -->
                <li class="template-layout-column-center-right" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/7.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/1b.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/1b.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Left column -->
                <li class="template-layout-column-left" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/8.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/8.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Center left column -->
                <li class="template-layout-column-center-left" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/11.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/11.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Center right column -->
                <li class="template-layout-column-center-right" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/12.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/12.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

                <!-- Right column -->
                <li class="template-layout-column-right" style="visibility: visible;">
                    <div class="template-component-image template-fancybox" style="background-image: none;">
                        <a href="img/_sample/1050x770/4B.jpg" data-fancybox-group="gallery-2" style="opacity: 1;">
                            <img src="img/_sample/1050x770/4B.jpg" alt="">
                            <span><span><span></span></span></span>
                        </a>
                    </div>
                </li>

            </ul>

        </div>
    </div>

    <div class="template-content-section template-padding-reset">

        <!-- Main -->
        <div class="template-main">
            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3 style=" font-size: 38px; line-height: 1.2em;"><b>Consulta la</b> nostra galeria de fotos!</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <a href="#" class="template-component-button template-component-button-style-1">Veure Galeria<i></i></a>

                    </div>

                </div>

            </div>

    </div>

    <!-- Section -->




    </div>

    <div class="template-content-section template-content-section-video">

        <!-- Box video -->
        <div class="template-component-box-video">

            <!-- Video -->
            <video autoplay="" loop="">
                <source src="http://quanticalabs.com/_other/video/fable.mp4" type="video/mp4">
                <source src="http://quanticalabs.com/_other/video/fable.webm" type="video/webm">
                <source src="http://quanticalabs.com/_other/video/fable.ogv" type="video/ogg">
            </video>

            <!-- Overlay -->
            <div></div>

        </div>


        <!-- Main -->
        <div class="template-main">

            <!-- White section -->
            <div class="template-section-white template-align-center">

                <!-- Header -->
                <h5 class="template-text-uppercase">t'ensenyem l'escola en un video</h5>

                <!-- Divider -->
                <div class="template-component-divider template-component-divider-style-3 template-margin-top-reset .template-margin-bottom-3"></div>

                <!-- Button -->
                <a href="#" class="template-component-button template-component-button-style-3">Mira aquest video<i></i></a>
<br><br>
                <a href="/p/videos.html"  <h6>Veure tots el videos</h6></a>


            </div>

        </div>

    </div>



    <!-- Section -->
    <div class="template-content-section template-padding-top-reset">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Que hi ha de nou</h2>
                <h6>Manteniu-vos al dia amb les últimes novetats</h6>
                <div></div>
            </div>

            <!-- Recent posts -->
            <div class="template-component-recent-post template-component-recent-post-style-1">

                <!-- Layout 33x33x33 -->
                <ul class="template-layout-33x33x33 template-clear-fix">

                    <!-- Left column -->
                    <li class="template-layout-column-left">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/7.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">12</div>
                        </div>
                        <h5><a href="#">Drawing and Painting Lessons</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Events</a>,
                                <a href="#">Fun</a>
                            </li>
                        </ul>
                    </li>

                    <!-- Center column -->
                    <li class="template-layout-column-center">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/2.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Fall Parents Meeting Day</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Dance</a>,
                                <a href="#">Education</a>
                            </li>
                        </ul>
                    </li>

                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-component-recent-post-date">September 20, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/9.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Birthday in Kindergarten</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Games</a>,
                                <a href="#">General</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>

            <div class="template-align-center">

                <!-- Button -->
                <a href="#" class="template-component-button template-component-button-style-1">Anar al Blog<i></i></a>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-reset">

        <div class="template-content-section template-padding-top-reset template-padding-bottom-reset">

            <!-- Google map -->
            <div class="template-component-google-map" id="template-component-google-map-1" data-lat="45.396011" data-lon="-75.676565" style="height: 400px; width: 100%; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div style="z-index: 1; position: absolute; top: 0px; left: 0px; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 544px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 288px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 544px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 544px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 800px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 288px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 288px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 800px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 800px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1056px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 32px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1056px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1056px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 32px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 32px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1312px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -224px; top: 38px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -224px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1312px; top: -218px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -224px; top: 294px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1312px; top: 294px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 544px; top: 38px;"><canvas draggable="false" height="256" width="256" style="user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 288px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 544px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 544px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 800px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 288px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 288px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 800px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 800px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1056px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 32px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1056px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1056px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 32px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 32px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1312px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -224px; top: 38px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -224px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1312px; top: -218px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -224px; top: 294px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1312px; top: 294px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 544px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18991!3i23472!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=88329" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 544px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18991!3i23471!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=11057" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 288px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18990!3i23472!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=42278" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 544px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18991!3i23473!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=34530" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 800px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18992!3i23472!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=3309" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 288px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18990!3i23473!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=119550" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 288px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18990!3i23471!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=96077" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 800px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18992!3i23471!4i256!2m3!1e0!2sm!3i394091283!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=60185" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1056px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18993!3i23472!4i256!2m3!1e0!2sm!3i394091283!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=52437" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 800px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18992!3i23473!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=80581" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 32px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18989!3i23472!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=1442" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1056px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18993!3i23473!4i256!2m3!1e0!2sm!3i394090851!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=109600" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1056px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18993!3i23471!4i256!2m3!1e0!2sm!3i394091283!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=106236" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 32px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18989!3i23473!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=78714" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1312px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18994!3i23472!4i256!2m3!1e0!2sm!3i394091283!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=98488" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 32px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18989!3i23471!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=55241" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -224px; top: 38px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18988!3i23472!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=86462" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -224px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18988!3i23471!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=9190" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1312px; top: -218px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18994!3i23471!4i256!2m3!1e0!2sm!3i394091283!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=21216" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -224px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18988!3i23473!4i256!2m3!1e0!2sm!3i394091295!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=32663" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1312px; top: 294px; transition: opacity 200ms ease-out;"><img draggable="false" alt="" src="http://maps.google.com/maps/vt?pb=!1m5!1m4!1i16!2i18994!3i23473!4i256!2m3!1e0!2sm!3i394090851!3m14!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC5zOi0xMDAscy50OjN8cy5lOmd8cC52OnNpbXBsaWZpZWR8cC5jOiNmZkZFOTM0QyxzLnQ6M3xzLmU6bC50LnN8cC5jOiNmZkZGRkZGRixzLnQ6NnxzLmU6Z3xwLmM6I2ZmOUE5NkM1LHMudDoyfHMuZTpsfHAudjpvZmYscy50OjN8cy5lOmwudC5mfHAuYzojZmY2NjY2NjYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6ODF8cC5jOiNmZkY1RjVGNSxzLnQ6ODJ8cC5jOiNmZkY1RjVGNSxzLnQ6MnxwLmM6I2ZmRThFOEU4!4e0&amp;token=24580" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"><div style="z-index: 1; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"></div></div><div style="z-index: 4; position: absolute; top: 0px; left: 0px; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=45.396011,-75.676565&amp;z=16&amp;t=m&amp;hl=es-ES&amp;gl=US&amp;mapclient=apiv3" title="Haz clic aquí para visualizar esta zona en Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google_white5.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 522px; top: 110px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Datos de mapas</div><div style="font-size: 13px;">Datos de mapas ©2017 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 232px; bottom: 0px; width: 157px;"><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Datos de mapas</a><span style="">Datos de mapas ©2017 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Datos de mapas ©2017 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 142px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/es-ES_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Términos de uso</a></div></div><div style="cursor: pointer; width: 25px; height: 25px; overflow: hidden; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Informar a Google acerca de errores en las imágenes o en el mapa de carreteras" href="https://www.google.com/maps/@45.396011,-75.676565,16z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Informar de un error de Maps</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="55" style="margin: 10px; user-select: none; position: absolute; bottom: 69px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 0px;"><div draggable="false" style="user-select: none; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;"><div title="Acerca la imagen" aria-label="Acerca la imagen" tabindex="0" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><div title="Aleja la imagen" aria-label="Aleja la imagen" tabindex="0" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Girar el mapa 90 grados" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; top: 0px; left: 614px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Muestra el callejero" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 28px; font-weight: 500;">Mapa</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; left: 0px; top: 32px; text-align: left; display: none;"><div draggable="false" title="Muestra el callejero con relieve" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Relieve</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Muestra las imágenes de satélite" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-left: 0px; min-width: 37px;">Satélite</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; right: 0px; top: 32px; text-align: left; display: none;"><div draggable="false" title="Muestra las imágenes con los nombres de las calles" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Etiquetas</label></div></div></div></div></div></div></div>

        </div>

    </div>

    <!-- Section -->
        <div class="template-content-section template-padding-top-reset template-background-image template-background-image-2">

            <!-- Main -->
            <div class="template-main template-section-white">

                <!-- Header and subheader -->
                <div class="template-component-header-subheader">
                    <h2>Contacte amb nosaltres</h2>
                    <h6>tens algun dubte o suggeriment?</h6>
                    <div></div>
                </div>

                <!-- Feature -->
                <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium" style="display: block;">

                    <!-- Layout 33x33x33 -->
                    <ul class="template-layout-33x33x33 template-clear-fix">

                        <!-- Left column -->
                        <li class="template-layout-column-left" style="visibility: visible;">
                            <div class="template-icon-feature template-icon-feature-name-envelope-alt template-icon-feature-size-medium"></div>
                            <h5>Adreça</h5>
                            <p>
                                Carrer Capellades, 2<br/>
                                08700 Igualada<br/>
                                Centre Concertat GC
                            </p>
                        </li>

                        <!-- Center column -->
                        <li class="template-layout-column-center">
                            <div class="template-icon-feature template-icon-feature-name-mobile-alt"></div>
                            <h5>Telèfon i email</h5>
                            <p>
                                Tel: 93 805 15 77<br/>
                                Fax: 93 805 15 77<br/>
                                <a href="mailto:office@fable.com">info@monalco.cat</a>
                            </p>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right template-margin-bottom-5">
                            <div class="template-icon-feature template-icon-feature-name-clock-alt"></div>
                            <h5>Horaris Secretaria</h5>
                            <p>
                                Dilluns a divendres<br/>
                                8.00 am – 5.00 pm<br/>
                                Cap de setmana tancat
                            </p>
                        </li>

                    </ul>

                </div>
                <!-- Contact form -->
                <div class="template-component-contact-form">

                    <form>

                        <!-- Layout 33x33x33 -->
                        <div class="template-layout-33x33x33 template-clear-fix">

                            <!-- Left column -->
                            <div class="template-layout-column-left" style="visibility: visible;">

                                <!-- Name -->
                                <div class="template-form-line template-state-block">
                                    <label for="contact-form-name">Nom *</label>
                                    <input type="text" name="contact-form-name" id="contact-form-name">
                                </div>

                            </div>

                            <!-- Center column -->
                            <div class="template-layout-column-center" style="visibility: visible;">

                                <!-- E-mail -->
                                <div class="template-form-line template-state-block">
                                    <label for="contact-form-email">E-mail *</label>
                                    <input type="text" name="contact-form-email" id="contact-form-email">
                                </div>

                            </div>

                            <!-- Right column -->
                            <div class="template-layout-column-right" style="visibility: visible;">

                                <!-- Subject -->
                                <div class="template-form-line template-state-block">
                                    <label for="contact-form-subject">Tema</label>
                                    <input type="text" name="contact-form-subject" id="contact-form-subject">
                                </div>

                            </div>

                        </div>

                        <!-- Layout 100 -->
                        <div class="template-layout-100 template-clear-fix">

                            <!-- Left column -->
                            <div class="template-layout-column-left" style="visibility: visible;">

                                <!-- Message -->
                                <div class="template-form-line template-state-block">
                                    <label for="contact-form-message">Missatge *</label>
                                    <textarea rows="1" cols="1" name="contact-form-message" id="contact-form-message"></textarea>
                                </div>

                            </div>

                            <!-- Left column -->
                            <div class="template-layout-column-left template-margin-bottom-reset" style="visibility: visible;">

                                <!-- Submit -->
                                <div class="template-form-line template-form-line-submit template-align-center">
                                    <div class="template-state-block">
                                        <input class="template-component-button template-component-button-style-2" type="submit" value="Enviar missatge" name="contact-form-submit" id="contact-form-submit">
                                    </div>
                                </div>

                            </div>

                        </div>

                    </form>

                </div>
            </div>

        </div>

</div>