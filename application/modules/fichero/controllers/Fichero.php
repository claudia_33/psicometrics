<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Fichero extends Panel{
        function __construct() {
            parent::__construct();
        }

        function folder_exists($folder){
            $path = realpath($folder);
            // If it exist, check if it's a directory
            if($path !== false AND is_dir($path))
            {
                // Return canonicalized absolute pathname
                return true;
            }
            // Path/folder does not exist
            return false;

        }

        function ficheros($user){
            if(!$this->folder_exists('files/users/'.$user)){
                mkdir('files/users/'.$user);
            }
            
        	$this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('ftp',array('user'=>$user),TRUE),'user'=>$user));
        }

        function verFtp($user,$connector = 0){
            if($connector==0){
                $this->load->view('_ftp',array('user'=>$user));
            }else{
                get_instance()->userid = $user;
                require_once APPPATH.'libraries/elfinder/connector.admin.php';                
            }
        }
    }
?>
