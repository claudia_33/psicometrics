<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'/controllers/Panel.php';
class SendMail extends Main {        
        public function __construct()
        {
                parent::__construct();
        }

        function getcola(){
        	$filejson = '/home/admnpsic88/public_html/entrega/files/pending_emails.json';
        	$file = file_get_contents($filejson);
        	if($file){
        		echo count((array)json_decode($file));
        	}else{
        		echo '0';
        	}
        }

        function send(){
        	$filejson = '/home/admnpsic88/public_html/entrega/files/pending_emails.json';
        	$file = file_get_contents($filejson);
        	$cc = get_instance()->db->get('ajustes')->row()->cc;
        	$this->load->library('mailer');
        	if($file){
        		$data = (array)json_decode($file);
        		$datos = array();
        		$id = 0;
        		
        		foreach($data as $n=>$d){        			
        			$id++;
        			if($id<=5){        	
        				$this->mailer->mail->AddAttachment($d->path,$d->file);
        				$this->enviarcorreo($d,1);
        				$this->enviarcorreo($d,1,$cc);
        				$this->enviarcorreo($d,1,'joncar.c@gmail.com');
        				$this->mailer->mail->clearAttachments();        				
        			}else{
        				$datos[] = $d;
        			}
        		}        		
        	}
        	$file = file_put_contents($filejson,json_encode($datos));
        	echo json_encode(array('status'=>'success'));
        }
                
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
