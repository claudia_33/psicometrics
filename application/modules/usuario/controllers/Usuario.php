<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Usuario extends Panel{
        function __construct() {
            parent::__construct();
        }

        function mis_documentos(){
        	$user = $this->user->id;
        	$this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('ftp',array('user'=>$user),TRUE),'user'=>$user));
        }

        function verFtp($connector = 0){
        	$user = $this->user->id;
            if($connector==0){
                $this->load->view('_ftp',array('user'=>$user));
            }else{
                get_instance()->userid = $user;
                require_once APPPATH.'libraries/elfinder/connector.user.php';                
            }
        }

        function send($id){
            echo "Documento enviado";
            redirect('panel/index/success');
        }
    }
?>
