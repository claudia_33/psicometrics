<!DOCTYPE html>
<!-- saved from url=(0024)http://wdpolis.eb2a.com/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content=""><!-- Description In Google-->
    <meta name="author" content=""> <!-- Name Mohamed Alaa-->
    <title>WDpolis</title><!-- Title In Tab Put Your Porject Name -->

    <!-- Bootstrap core CSS -->
    <link href="./WDpolis_files/bootstrap.css" rel="stylesheet">
    <!-- Font-awesome -->  
    <link href="./WDpolis_files/font-awesome.css" rel="stylesheet"> 
    <!-- Google Font  -->      
    <link href="./WDpolis_files/css" rel="stylesheet">
    <!-- Google Font  -->
    <!-- magnific-popup  -->
    <link href="./WDpolis_files/magnific-popup.css" rel="stylesheet">  
    <!-- style styles for this template -->
    <link href="./WDpolis_files/style.css" rel="stylesheet">
    <link id="colorchoose" href="http://wdpolis.eb2a.com/" rel="stylesheet">
      
    <!-- responsive styles for this template -->
    <link href="./WDpolis_files/responsive.css" rel="stylesheet">  

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="">
      
    <!-- Start Loading -->
    <div class="body" style="display: none;">
        <div class="table-loading">
          <div class="sk-cube-grid">
              <div class="sk-cube sk-cube1"></div>
              <div class="sk-cube sk-cube2"></div>
              <div class="sk-cube sk-cube3"></div>
              <div class="sk-cube sk-cube4"></div>
              <div class="sk-cube sk-cube5"></div>
              <div class="sk-cube sk-cube6"></div>
              <div class="sk-cube sk-cube7"></div>
              <div class="sk-cube sk-cube8"></div>
              <div class="sk-cube sk-cube9"></div>
          </div>
        </div>    
    </div>   
    <!-- Start Loading -->
      
   <!-- Start navbar -->
    <nav class="navbar navbar-default" id="nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation WDpolis</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Name Of Porject For You Change Up To You 
                EX:<a class="navbar-brand" href="#">name<small>name</small></a>           
          -->    
          <a class="navbar-brand smoothScroll" href="http://wdpolis.eb2a.com/#home">WD<small>polis</small></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">  
          <ul class="nav navbar-nav navbar-right">
            <li><a id="navhome" href="http://wdpolis.eb2a.com/#homepage">Home</a></li>
            <li><a id="navabout" href="http://wdpolis.eb2a.com/#aboutpage">About</a></li>
            <li><a id="navservice" href="http://wdpolis.eb2a.com/#servicepage">service</a></li>              
            <li><a id="navportfolio" href="http://wdpolis.eb2a.com/#portfoilopage">work</a></li>
            <li><a id="navcontact" href="http://wdpolis.eb2a.com/#contactpage">Contact</a></li>
          </ul>
        </div> 
        <!--/.nav-collapse -->
      </div>
    </nav>
    <!-- Start navbar --> 
      
   <!-- Start Home Page -->
   <section class="homePage" id="home" style="height: 638px;">
     <div class="container">
        <div class="row overflownone">
          <div class="col-md-8 nopadding-home">
              <div class="slideshow">
                  <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel" style="height: 638px;">
                      <!-- Wrapper for slides All Image OF Slidre -->
                      <div class="carousel-inner" role="listbox">
                          <!-- Image (1) Edit EX:style = "background-image: url(image/name Of Image );"-->
                          <div class="item next left" style="background-image: url(images/33.jpg);">
                          </div>
                          <!-- Image (2) Edit EX:style = "background-image: url(image/name Of Image );"-->
                          <div class="item" style="background-image: url(images/11.jpg);">
                          </div>
                          <!-- Image (3) Edit EX:style = "background-image: url(image/name Of Image );"-->
                          <div class="item active left" style="background-image: url(images/12.jpg);">
                          </div>
                     </div>
                  </div>
              </div>
          </div>
        </div>
    </div><!-- Close Container -->
     <div class="carousel-caption" style="padding-top: 277.391px;">
         <!-- Text Home Page Edit ( WDpolis ) For Name Website -->
         <h1> START YOUR BUSNIESS WITH WDpolis</h1>
         <!-- Text Home Page Postion Front End, back End Developer, photograph Up To You Eny Postion  -->
         <h5>Professional Web Developer &amp; Front End Developer.</h5>
         <!-- Button Go TO Contact -->
         <a class="btn btn-WDpolis" id="button-go-contact" href="http://wdpolis.eb2a.com/#contactpage">Let's Talk</a>
     </div>
   </section>
   <!-- End Home Page -->
       
   <!-- Start about -->
   <section class="aboutpage" id="about" style="height: 564px;">
       <div class="container">
           <div class="row">
               <!-- Header For About  -->
               <h2>who we are</h2>
               <div class="col-md-8 col-sm-8 col-xs-12 text">
                   <!-- Pragraph In Home Page -->
                   <p class="sub-p">
                       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam  vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Seddfvac blandit elit tincidunt id. Seddfv rhoncus, tortor sed eleifend rhoncus, tortor sed eleifend
                   </p>
                   <!-- Accordion Start -->
                   <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <!-- Accordion (1) -->
                       <div class="panel panel-default">
                           <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="http://wdpolis.eb2a.com/#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-files-o" aria-hidden="true"></i>
                                        <!-- Text (1) in Accordion -->
                                        Multiple Concept Design
                                    </a>
                                </h4>
                            </div>
                           <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <!-- Pragraph (1) For Accordion -->
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui ligula, blandit non maximus id, tempus vitae nulla. Cras nec turpis vitae libero eleifend consequat. Phasellus eu rutrum erat. Etiam id urna sed odio placerat gravida. Integer molestie lectus eu.
                                    </p>
                                </div>
                            </div>
                       </div>
                        <!-- Accordion (2) -->                       
                       <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="http://wdpolis.eb2a.com/#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-code" aria-hidden="true"></i>
                                        <!-- Text (2) in Accordion -->
                                        Clean Code
                                    </a>
                                </h4>
                            </div>
                           <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                               <!-- Body Of Section (2) -->
                               <div class="panel-body">
                                   <!-- Pragraph (2) in Accordion -->
                                  <p>
                                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui ligula, blandit non maximus id, tempus vitae nulla. Cras nec turpis vitae libero eleifend consequat. Phasellus eu rutrum erat. Etiam id urna sed odio placerat gravida. Integer molestie lectus eu.
                                   </p>
                               </div>
                           </div>
                        </div>
                        <!-- Accordion (3) -->
                       <div class="panel panel-default">
                           <!-- Name Of Section One -->
                           <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="http://wdpolis.eb2a.com/#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-html5" aria-hidden="true"></i>
                                        <!-- Text (2) in Accordion -->
                                        Html5 and Css3
                                    </a>
                                </h4>
                          </div>
                           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                               <!-- Body Of Section (3) -->
                               <div class="panel-body">
                                     <!-- Pragraph (3) in Accordion -->
                                     <p>
                                         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui ligula, blandit non maximus id, tempus vitae nulla. Cras nec turpis vitae libero eleifend consequat. Phasellus eu rutrum erat. Etiam id urna sed odio placerat gravida. Integer molestie lectus eu.
                                    </p>
                                </div>
                            </div>
                       </div>
                 </div>
                  <!-- End Accordion -->   
             </div>
               <!-- Image About Page -->
               <div class="col-md-4 col-sm-4 col-xs-12">
                   <!-- Image In About You Can Change  
                        Edit( src="NameImge.jpg")-->
                   <img class="img-responsive" src="./WDpolis_files/man8.jpg" alt="person">
                </div>
             </div> 
       </div>
   </section>
   <!-- End about -->
       
   <!-- Start Service -->
   <section class="servicepage" id="service">
        <div class="container">
          <div class="row">
              <!-- Header In Service Page -->
              <h2>Service</h2>
              <!-- Service (1)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service">
                     <div class="top-body">
                         <i class="fa fa-laptop fa-2x" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3> WEB DESIGN &amp; UI</h3>
                     </div>   
                    <!-- Description Of Title Service -->  
                    <p>
                     Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
              <!-- Service (2)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service">
                     <div class="top-body">
                        <i class="fa fa-object-ungroup fa-2x" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3>WIREFRAMES</h3>
                     </div>
                    <!-- Description Of Title Service -->   
                    <p>
                      Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
              <!-- Service (3)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service active-body">
                     <div class="top-body">
                         <i class="fa fa-paint-brush fa-2x active" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3 class="active-h3">VISUAL DESIGN</h3>
                     </div> 
                    <!-- Description Of Title Service -->   
                    <p>
                       Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
              <!-- Service (4)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service">
                     <div class="top-body">
                         <i class="fa fa-first-order fa-2x" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3>LOGO DESIGN</h3>
                     </div> 
                    <!-- Description Of Title Service -->   
                    <p>
                       Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
              <!-- Service (5)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service">
                     <div class="top-body">
                         <i class="fa fa-android fa-2x" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3> MOBILE DESIGN</h3>
                     </div> 
                    <!-- Description Of Title Service -->   
                    <p>
                       Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
              <!-- Service (6)-->
              <div class="col-md-4 col-sm-6">
                  <div class="body-service">
                     <div class="top-body">
                         <i class="fa fa-clone fa-2x" aria-hidden="true"></i>
                         <!-- Title Of Service -->
                        <h3>PRINT DESIGN</h3>
                     </div> 
                    <!-- Description Of Title Service -->   
                    <p>
                      Lorem ipsum dolor consectetur ubique lit adipiscing ellit, ubiques maluisset ubique elit, ubique maluisset vel te,lit vituperata adipiscing.
                    </p>
                  </div>  
                </div>
          </div>
       </div>
   </section>
   <!-- End Service -->
       
   <!-- Start portfolio -->
   <section class="portfoliopage" id="portfolio" style="height: 564px;">
        <div class="container">
            <!-- Filter Controls - Simple Mode -->
            <div class="row">
                <!-- Header Work Page -->
                <h2>work.</h2>
                <!--All Project -->
                <div class="filtr-container">
                    <!-- Work (1)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/city_1.jpg" alt="sample image">
                        <div class="div-hover">
                           <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/city_1.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                           </a>
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (2)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/nature_2.jpg" alt="sample image">
                        <div class="div-hover"> 
                           <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/nature_2.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                           </a> 
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (3)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/city_3.jpg" alt="sample image">
                        <div class="div-hover">
                           <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/city_3.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                            </a>
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (4)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/industrial_1.jpg" alt="sample image">
                        <div class="div-hover"> 
                            <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/industrial_1.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                           </a> 
                           <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (5)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/industrial_2.jpg" alt="sample image">
                        <div class="div-hover">
                           <!-- Edit:href ="nameimage.jpg" -->    
                           <a href="./WDpolis_files/industrial_2.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                           </a> 
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (6)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/nature_1.jpg" alt="sample image">
                        <div class="div-hover"> 
                            <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/nature_1.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                            </a> 
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (7)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/city_2.jpg" alt="sample image">
                        <div class="div-hover"> 
                            <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/city_2.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i> 
                           </a> 
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                     <!-- Work (8)-->
                    <div class="col-xs-12 col-sm-4 col-md-3 filtr-item">
                        <!-- Edit:src ="nameimage.jpg" -->
                        <img class="img-responsive" src="./WDpolis_files/nature_3.jpg" alt="sample image">
                        <div class="div-hover"> 
                           <!-- Edit:href ="nameimage.jpg" -->
                           <a href="./WDpolis_files/nature_3.jpg" class="image-popup-vertical-fit">
                               <i class="fa fa-search fa-lg" aria-hidden="true"></i>
                           </a> 
                            <!-- Link For Project EX: href="www.google.com" -->
                           <a href="http://wdpolis.eb2a.com/#"><i class="fa fa-link fa-lg" aria-hidden="true"></i></a> 
                        </div>
                        <!-- Name Of Project -->
                        <span class="item-desc">Project Title</span>
                    </div>
                </div>
            </div><!-- Close Row -->
        </div>
   </section>
   <!-- End portfolio -->
       
   <!-- Start Contact -->
   <section class="contactpage" id="contact" style="height: 564px;">
        <div class="container">
           <div class="row">
               <!-- Header Contact Page -->
                <h2>Contact.</h2> 
               <!-- Text In Contact -->
               <h3 class="text-center"> LEAVE ME A MESSAGE . . .</h3>
               <!-- Text Pragraph -->
               <p class="text-center"> 
                   You can ask me about anything here, just type your name and e-mail and I'll answer you as soon as possible.  
               </p>
               <!-- Start Form -->
               <form>
                   <div class="form-group col-md-6 col-sm-6">
                    <div class="input-group">  
                      <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span> 
                      <!-- Input Frist Name -->    
                      <input type="text" class="form-control" id="exampleInputFristName" placeholder="Frist Name" required="">
                    </div>
                  </div>
                   <div class="form-group col-md-6 col-sm-6">
                    <div class="input-group">  
                        <span class="input-group-addon">
                            <i class="fa fa-users fa" aria-hidden="true"></i>
                        </span>
                        <!-- Input Last Name -->
                        <input type="text" class="form-control" id="exampleLastName" placeholder="Last Name" required="">
                    </div>
                  </div>
                   <div class="form-group col-md-6 col-sm-6">
                    <div class="input-group">  
                        <span class="input-group-addon">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </span>
                      <!-- Input phone -->
                      <input type="text" class="form-control" id="exampleInputPassword1" placeholder="phone" required="">
                    </div>
                  </div>
                   <div class="form-group col-md-6 col-sm-6">
                    <div class="input-group">  
                        <span class="input-group-addon">
                            <i class="fa fa-envelope fa" aria-hidden="true"></i>
                        </span>
                      <!-- Input Email -->
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required="">
                    </div>
                  </div>
                   <div class="form-group col-md-12">
                     <!-- Textarea -->
                       <textarea class="form-control" rows="5" placeholder="Enter Message..."></textarea>
                   </div>   
                   <!-- Button Submit -->
                   <button type="submit" class="btn-default-polis">Submit</button>
               </form><!-- Close Form -->
           </div>
        </div>
   </section>
   <!-- End Contact -->
       
   <!-- Start Footer -->  
   <footer>
        <div class="container">
            <!-- Start Row -->
            <div class="row">
                <div class=" text-center">
                    <p>Copyright © 2017. All rights reserved Designed By 
                        <a href="https://themeforest.net/user/m_alaa/portfolio" target="" _blank=""> Mohamed Alaa </a></p>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- End Container -->
   </footer>   
   <!-- End Footer -->  
       
    <!-- Button Show In Scroll To Scroll Top -->
    <a href="http://wdpolis.eb2a.com/#nav" class="btn btn-lg icon-top smoothScroll" id="top-button">
        <i class="fa fa-sort-asc" aria-hidden="true"></i>
    </a>
    <!-- End Button -->
    <!-- Start Switch -->
      <section class="switch">
            <div class="container-switch">
                <div id="div2"></div>
                <div id="div3"></div>
                <div id="div4"></div>
                <div id="div5"></div>
           </div>
           <div id="icon-switch" class="icon-switch">
              <a><i class="fa fa-cog" aria-hidden="true"></i></a>  
           </div>
      </section>
    <!-- Start Switch -->

    <!-- JQuery.js -->  
    <script src="./WDpolis_files/jquery-3.1.1.min.js.descarga"></script>
    <script src="./WDpolis_files/1.12.0.jquery.min.js.descarga"></script>
    <!-- bootstrap.min.js -->
    <script src="./WDpolis_files/bootstrap.min.js.descarga"></script>
    <!-- html5shiv.js -->
    <script src="./WDpolis_files/html5shiv.js.descarga"></script>
    <!-- smoothscroll.js -->
    <script src="./WDpolis_files/smoothscroll.js.descarga"></script>
    <!-- magnific-popup.min.js -->
    <script src="./WDpolis_files/jquery.magnific-popup.js.descarga"></script>
    <!-- main.js -->
    <script src="./WDpolis_files/main.js.descarga"></script> 
    <!-- main.js -->
    <script src="./WDpolis_files/switch.js.descarga"></script> 
      
  

<div style="text-align: center;"><div style="position:relative; top:0; margin-right:auto;margin-left:auto; z-index:99999">

</div></div>
</body></html>