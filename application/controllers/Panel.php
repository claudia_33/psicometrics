<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        CONST CURSANDO = 1;//Estado de inscripcion
        CONST RECURSANDO = 3;//Estado de inscripcion
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view' => 'main'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }            
            else{
                if(!empty($param->output)){
                    $param->view = empty($param->view)?'panel':$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                parent::loadView($param);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('template');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }

        function index(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            if($this->user->admin==1){   
                $crud->set_table('user');            
                $crud->set_subject('Usuarios');
                $crud->field_type('password','password')
                     ->field_type('admin','hidden',0)
                     ->field_type('status','true_false',array('0'=>'Bloquedo','1'=>'Activo'));
                $crud->set_field_upload('foto','img/fotos');
                $crud->required_fields_array();
                $crud->add_action('<i class="fa fa-file"></i> Documentos','',base_url('fichero/ficheros').'/');
                $crud->callback_before_insert(function($post){                    
                    $post['password'] = md5($post['password']);
                    return $post;
                });
                $crud->callback_after_insert(function($post,$primary){                    
                    get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>3));
                    mkdir('files/users/'.$primary);
                    mkdir('files/users/'.$primary.'/PDFS');
                    return $post;
                });
                $crud->callback_before_update(function($post,$primary){
                    $user = get_instance()->db->get_where('user',array('id'=>$primary));
                    if($user->row()->password!=$post['password']){
                        $post['password'] = md5($post['password']);
                    }
                    return $post;
                });      
                $crud->callback_before_delete(function($primary){
                    $user = get_instance()->db->delete('user_group',array('user'=>$primary));
                    if(is_dir('files/users/'.$primary)){
                        $dirname = 'files/users/'.$primary;
                        array_map('unlink', glob("$dirname/*.*"));
                        rmdir($dirname);
                    }
                });  
                $crud->columns('nombre','apellido','email','status');          
            }else{
                redirect('usuario/mis_documentos');
            }
            $output = $crud->render();
            $this->loadView($output);
        }
                
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
