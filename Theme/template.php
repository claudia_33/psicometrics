<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= !empty($title)?$title:'Psicometrix' ?></title>
        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/normalize.css">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/css/bootstrap-select.less"> -->
        <?php 
            if(!empty($css_files) && !empty($js_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?> 
        <?php endif; ?>

        <?php 
        if(!empty($css_files) && !empty($js_files)):
        foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
        <?php endforeach; ?>                
        <?php endif; ?>
        
        <link rel="stylesheet" href="<?= base_url() ?>Theme/assets/scss/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        
        
    </head>
    <body class="bg-lite">
        <?= $this->load->view($view) ?>        
    </body>
</html>
