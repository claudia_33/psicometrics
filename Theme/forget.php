<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
        <?= !empty($msj)?$msj:'' ?>
        <div class="login-content">
            <div class="login-logo">
                <a href="<?= base_url() ?>">
                    <img class="align-content" src="<?= base_url() ?>Theme/images/Original.png" alt="">
                </a>
                <h1>Reestablecer Contraseña</h1>
            </div>
            <div class="login-form">
                <form action="<?= base_url('registro/forget') ?>" method="post">
                    <h4>Ingresa tu correo electrónico y te llegarán instrucciones para reestablecer tu contraseña</h4>
                    <div class="form-group">
                        <label>Correo electrónico</label>
                        <input type="email" name="email" class="form-control" placeholder="Correo electrónico">
                    </div>
                    <button type="submit" class="btn btn-primary btn-flat m-b-15">Enviar</button>
                    <a href="<?= base_url() ?>" target="_self">Regresar</a>
                </form>
            </div>
        </div>
    </div>
</div>