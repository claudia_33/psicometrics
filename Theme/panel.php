<!-- Right Panel -->
        <div id="right-panel" class="right-panel">
            
            <!-- Header-->
            <header id="header" class="header h-usuario">
                <div class="header-menu">
                    <div class="col-lg-6 pull-left">
                        <a href="<?= base_url('panel') ?>">
                            <figure>
                                <img src="<?= base_url() ?>Theme/images/Negativo.png">
                            </figure>
                        </a>
                    </div>
                    
                    <div class="col-lg-6 pull-right">
                        <div class="user-area dropdown float-right">
                            <?php 
                                                $emptyphoto = 'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png';
                                                if(!empty($this->user->facultadLogo)){
                                                    $emptyphoto = 'img/fotos_facultades/'.$this->user->facultadLogo;
                                                }
                                            ?>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="user-avatar rounded-circle" src="<?= base_url(empty($this->user->foto)?$emptyphoto:'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>">
                            </a>

                            <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>"><i class="fa fa -cog"></i>Perfil</a>
                                <?php if($this->user->admin==1): ?><a class="nav-link" href="<?= base_url('seguridad/ajustes') ?>"><i class="fa fa-cog"></i>Configuraciones</a><?php endif ?>
                                <?php if($this->user->admin==1): ?><a class="nav-link" href="<?= base_url('notificaciones/admin/notificaciones') ?>"><i class="fa fa-cog"></i>Notificaciones</a><?php endif ?>
                                <a class="nav-link" href="<?= base_url('main/unlog') ?>"><i class="fa fa-power -off"></i>Salir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Header-->

            <div class="breadcrumbs">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Bienvenido <?= $this->user->nombre ?></h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content mt-3">
                <div class="animated fadeIn">
                    <?= !empty($output)?$output:'' ?>                    
                </div><!-- .animated -->
            </div> <!-- .content -->
            
        </div><!-- /#right-panel -->

        <!-- Right Panel -->
        
        <!-- MODALES -->
        <!-- MODAL -->
        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Documento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <iframe id="verFichero" src=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- TERMINA MODAL -->
        <script src="<?= base_url() ?>Theme/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>Theme/assets/js/plugins.js"></script>
        <script src="<?= base_url() ?>Theme/assets/js/main.js"></script>
        
        <?php 
        if(!empty($css_files) && !empty($js_files)):
        foreach($js_files as $file): if(!strpos($file,'flexigrid')): ?>

            <script src="<?= $file ?>"></script>
        <?php endif; endforeach; ?>                
        <?php endif; ?>
           

        <script>
            function mostrarFichero(url){
                $("#verFichero").attr('src',url);
                $("#scrollmodal").modal('toggle');
            }
        </script>