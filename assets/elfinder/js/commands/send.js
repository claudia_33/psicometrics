/**
 * @class  elFinder command "view"
 * Change current directory view (icons/list)
 *
 * @author Dmitry (dio) Levashov
 **/
elFinder.prototype.commands.send = function() {	
	"use strict";
	var fm = this.fm;
	this.value          = 'send';
	this.alwaysEnabled  = false;
	this.updateOnSelect = false;	
	
	this.getstate = function(select) {
		return 0;
	};
	
	this.exec = function(select) {
		var hashes  = this.hashes(select),
			fm      = this.fm;		
		if(confirm('Seguro que desea enviar los emails a los destinatarios descritos en el excel?')){			
			var dialogText = '<div id="sendPathEmails">Enviando a los destinatarios por favor espere, este proceso puede demorar</div>';
			var dialog = fm.dialog(dialogText, {
				title          : '<span>Enviando</span>',
				modal          : true,
				resizable      : false,
				destroyOnClose : true,
				close          : function() {
					var cm = fm.getUI('contextmenu');
					if (cm.is(':visible')) {
						cm.click();
					}
				}
			});

			fm.request({
				data   : {cmd : 'send', targets : this.hashes(select)},
				notify : {type : 'copy', cnt : 1},
				navigate : {
					toast : {
						inbuffer : {msg: 'Proceso de envio exitoso'}
					}
				}
			}).done(function(data){
				$("#sendPathEmails").html(data.msg);

				
			});	
		}

		return $.Deferred().resolve();
	};

};
