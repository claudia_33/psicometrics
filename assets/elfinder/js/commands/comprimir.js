/**
 * @class  elFinder command "view"
 * Change current directory view (icons/list)
 *
 * @author Dmitry (dio) Levashov
 **/
elFinder.prototype.commands.comprimir = function() {	
	"use strict";
	var fm = this.fm;
	this.value          = 'send';
	this.alwaysEnabled  = false;
	this.updateOnSelect = false;	
	
	this.getstate = function(select) {
		return 0;
	};
	
	this.exec = function(select) {
		var hashes  = this.hashes(select),
			fm      = this.fm;		

		var dialogText = '<div style="background: lightgreen;border: 1px solid green;padding: 10px;margin: 10px;width: 100%;text-decoration: none;" id="sendPathEmails">Generando zip por favor espere</div>';
		var dialog = fm.dialog(dialogText, {
			title          : '<span>Generando ZIP</span>',
			modal          : true,
			resizable      : false,
			destroyOnClose : true,
			close          : function() {
				var cm = fm.getUI('contextmenu');
				if (cm.is(':visible')) {
					cm.click();
				}
			}
		});

		fm.request({
			data   : {cmd : 'comprimir', targets : this.hashes(select)},
			notify : {type : 'copy', cnt : 1},
			navigate : {
				toast : {
					inbuffer : {msg: 'Proceso de envio exitoso'}
				}
			}
		}).done(function(data){
			$("#sendPathEmails").html('<a href="'+data.url+'" target="_blank">Descargar ZIP</a>');
		});	

		return $.Deferred().resolve();
	};

};
